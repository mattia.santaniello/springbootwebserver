package com.example.service;

import com.example.repository.AttoreRepository;
import com.example.domain.Attore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

//implementa la logica di business
@Service //logica di business
public class AttoreService {
    @Autowired //Dependency Injection
    AttoreRepository attoreRepository; 


    
    public List<Attore> findAll(){ // restituisce tutti gli attori della tabella
        return attoreRepository.findAll();
    }

    //Optional<Attore> è un oggetto che ci consente di controllare se l'attore è nel DB
    public Optional<Attore> findById(Long codAttore){
        return attoreRepository.findById(codAttore);
    }

    public Attore save(Attore attore){
        return attoreRepository.save(attore);
    }

    public Optional<Attore> deleteById(Long codAttore){
        Optional<Attore> optAttore = attoreRepository.findById(codAttore);
        if(optAttore.isPresent()){ //se l'attore risulta essere presente allora restituisce optAttore dopo la cancellazione
            attoreRepository.deleteById(codAttore);
            return optAttore;
        }
        return Optional.empty(); //nel caso in cui non sia entrato nell'if restituisce Optional.empty()
    }

    public Optional<Attore> put(Attore attore){
        Optional<Attore> optAttore = attoreRepository.findById(attore.getCodAttore());
        if(optAttore.isPresent()){
            attoreRepository.save(attore);
            return optAttore;
        }else{
            return optAttore.empty();
        }
    }

    public List<Attore> findAllPage(Pageable paegable){
        List<Attore> l = attoreRepository.findAll(paegable).getContent();
        return l;
    }
}
