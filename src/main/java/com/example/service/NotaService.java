package com.example.service;

import com.example.domain.Attore;
import com.example.domain.Nota;

import com.example.repository.NotaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.List;

@Service
public class NotaService {
    @Autowired
    NotaRepository notaRepository;

    @Autowired
    AttoreService attoreService;

    public void save(Nota nota,Long codAttore){
        notaRepository.save(nota);
        Optional<Attore> optAttore = attoreService.findById(codAttore);
        if(optAttore.isPresent()){
            Attore attore = optAttore.get();
            attore.setNota(nota);
            attoreService.save(attore);
        }
    }

    public List<Nota> getAll(){
        return notaRepository.findAll();
    }
}
