package com.example.domain;

import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name="note")
public class Nota{
    @Id
    @Column(name="id")
    Long id;

    @OneToOne(mappedBy="nota")
    Attore attore;

    @Lob
    String note;
}