package com.example.domain;
import com.example.domain.Country;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityReference;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter 
@Table(name="attori")
@Setter
@EqualsAndHashCode
//la classe Attore serve per interagire con il DB usando la logica OOP

public class Attore{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //auto increment
    @Column(name="cod_attore")
    Long codAttore;

    
    String nome;
    
    @Column(name="anno_nascita")
    Long annoNascita;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="country")
    @JsonIdentityReference //prelievo dati da Country
    Country country;


    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "nota_id",referencedColumnName = "id")
    Nota nota;
    
}