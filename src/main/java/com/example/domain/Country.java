package com.example.domain;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString; 
import java.util.Set;


@Entity
@Getter
@Setter
@Table(name="countries")


@ToString
public class Country {
    @Id
    @Column(name="country",length = 20,nullable=false)
    String country;

    @OneToMany(
        mappedBy = "country",
        cascade = CascadeType.ALL,
        orphanRemoval = true 
    )
    @JsonIgnore
    Set<Attore> attore;
}
