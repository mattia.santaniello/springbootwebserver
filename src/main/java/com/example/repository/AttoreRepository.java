package com.example.repository;

import com.example.domain.Attore;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//crea un interfaccia che consente di implementare i metodi per l'interrogazione del DB
@Repository
public interface AttoreRepository extends JpaRepository<Attore,Long>{
    
}
