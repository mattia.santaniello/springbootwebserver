package com.example.component;

import org.springframework.stereotype.Component;

@Component //permette di implementare 
public class Prova {
    String titolo="ciao";

    public Prova() {
    }

    public Prova(String titolo) {
        this.titolo = titolo;
    }

    public String getTitolo() {
        return this.titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public Prova titolo(String titolo) {
        setTitolo(titolo);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " titolo='" + getTitolo() + "'" +
            "}";
    }

}
