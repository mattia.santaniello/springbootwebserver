package com.example.controller;

import org.springframework.web.bind.annotation.RestController;

import com.example.service.AttoreService;
import com.example.component.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import com.example.domain.Attore;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.Optional;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;



//la classe controller gestisce le richieste REST come PUT,POST,DELETE,GET

@RestController
@RequestMapping("/api/attori") //endpoint
@CrossOrigin(origins = "*",maxAge=3600)
public class AttoreController {
    //@Autowired //Dependency Injection
    AttoreService attoreService;
    Prova prova;

    public AttoreController(AttoreService attoreService,Prova prova){ //altro modo per fare la Dependency Injection senza autowired
        this.attoreService=attoreService;
        this.prova=prova;
    }


    
    /*@GetMapping() //quando non viene passato alcun parametro nell'URL, viene usato questo metodo
    public ResponseEntity<?> findAll(){
        return  new ResponseEntity<List<Attore>>(attoreService.findAll(),HttpStatus.OK);
    }*/
    

    @GetMapping() //quando non viene passato alcun parametro nell'URL, viene usato questo metodo
    public ResponseEntity<?> findAll(
        @RequestParam(required = false,name="page",defaultValue="0") int page,
        @RequestParam(required = false,name="size",defaultValue="10") int size
    ){
        String field = "codAttore";
        String ascdesc = "asc";
        Sort sort = Sort.by(field);

        if(ascdesc.equals("desc")){
            sort = sort.descending();
        }
    
        Pageable request =  PageRequest.of(page,size,sort);
        return  new ResponseEntity<List<Attore>>(attoreService.findAll(),HttpStatus.OK);
    }


    @PostMapping("/dto")
    public void dtio(@RequestBody AttoriPar par){
        System.out.println(par);
    }

    //si implementa il percorso {codAttore}, il quale quest'ultimo è 
    //l'id dell'attore che stiamo cercando. Con PathVariable possiamo "catturare" il valore
    //del parametro
    @GetMapping("/{codAttore}") 
    public ResponseEntity<?> findByID(@PathVariable("codAttore") Long codAttore){
        //return new ResponseEntity<Attore>(attoreService.findByID(codAttore),HttpStatus.OK);

        Optional<Attore> optAttore = attoreService.findById(codAttore);
        if(optAttore.isPresent()){ //controlla se l'attore è stato trovato
            Attore attore = optAttore.get();
            return new ResponseEntity<Attore>(attore,HttpStatus.OK);
        }

        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }


    @GetMapping("/c")
    public ResponseEntity<?> c(){
        return new ResponseEntity<String>(prova.getTitolo(),HttpStatus.OK);
    }

    // in questo metodo save possiamo estrarre il body della richiesta
    @PostMapping() //esegue il metodo save quando la richiesta è POST
    public ResponseEntity<?> save(@RequestBody Attore attore){
        return new ResponseEntity<Attore>(attoreService.save(attore),HttpStatus.OK);
    }

    @DeleteMapping("/{codAttore}")
    public ResponseEntity<?> deleteByID(@PathVariable("codAttore") Long codAttore){
        //return new ResponseEntity<Attore>(attoreService.findByID(codAttore),HttpStatus.OK);

        Optional<Attore> optAttore = attoreService.deleteById(codAttore);
        if(optAttore.isPresent()){ //controlla se l'attore è stato trovato
            Attore attore = optAttore.get();
            return new ResponseEntity<Attore>(attore,HttpStatus.OK);
        }

        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }


    @PutMapping()
    public ResponseEntity<?> put(@RequestBody Attore attore){
        Optional<Attore> optAttore=attoreService.put(attore);
        if(optAttore.isPresent()){
            return new ResponseEntity<Void>(HttpStatus.OK);
        }

        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }
}
