package com.example.controller;

import org.springframework.web.bind.annotation.RestController;

import com.example.service.AttoreService;
import com.example.service.NotaService;
import com.example.component.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import com.example.domain.Nota;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.Optional;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

@RestController
@RequestMapping("/api/note") //endpoint
@CrossOrigin(origins = "*",maxAge=3600)
public class NotaController {
    @Autowired
    NotaService notaService;

    @GetMapping(value="/")
    public ResponseEntity<List<Nota>> getAll() {
        return new ResponseEntity<List<Nota>>(notaService.getAll(),HttpStatus.OK);
    }
    
}
