DROP DATABASE If EXISTS lezione08032021;
CREATE DATABASE if not EXISTS lezione08032021;
use lezione08032021;


create table if not exists note(
    Id BIGINT(20) NOT NULL,
    note LONGTEXT NULL DEFAULT NULL,
    constraint pk_id primary key (id)
);
create table countries(
    country VARCHAR (20),
    constraint pk_country PRIMARY Key (country)
);

create table if not exists attori(
    cod_attore BIGINT  AUTO_INCREMENT PRIMARY KEY,
    anno_nascita BIGINT(20) ,
    country varchar(20),
    nome varchar(255),
    nota_id BIGINT(20),
    constraint fk_nota_id_note foreign key (nota_id) REFERENCES note(id),
    constraint fk_country_attori FOREIGN key (country) REFERENCES countries(country)
);